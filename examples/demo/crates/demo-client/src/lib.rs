#![deny(bare_trait_objects)]
#![warn(clippy::all)]

use {log::Level, wasm_bindgen::prelude::wasm_bindgen};

#[wasm_bindgen(start)]
pub fn start() {
    console_error_panic_hook::set_once();
    console_log::init_with_level(Level::Debug).unwrap();

    let init = demo_app::AppInit;
    blast_app_browser::start(&init);
}
