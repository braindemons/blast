use {
    blast::{
        event::Context,
        render::{el, ComponentExt, RenderNode},
        Component,
    },
    blast_fetch::fetch,
    blast_markdown::MarkdownExt,
    futures::Future,
    log::info,
    serde::Deserialize,
};

#[derive(Deserialize)]
struct ArticlesResponse {
    articles: Vec<Article>,
}

#[derive(Deserialize, Clone)]
pub struct Article {
    title: String,
    body: String,
    #[serde(rename = "tagList")]
    tag_list: Vec<String>,
    author: Author,
}

#[derive(Deserialize, Clone)]
pub struct Author {
    username: String,
}

pub struct ArticleList {
    articles: Option<Vec<Article>>,
}

impl Component for ArticleList {
    type Props = ();

    fn new(_: ()) -> Self {
        Self { articles: None }
    }

    fn on_fetch(ctx: Context<Self>) {
        info!("Fetching Articles");

        let ctx_0 = ctx.clone();
        let future = fetch("http://conduit.productionready.io/api/articles").map(
            move |data: ArticlesResponse| {
                info!("Finished Fetching Articles");
                let articles = data.articles;
                ctx_0.set(|c| c.articles = Some(articles));
            },
        );

        ctx.dispatch(future)
    }

    fn render(&self) -> RenderNode {
        let articles = self
            .articles
            .as_ref()
            .map(|articles| {
                articles
                    .iter()
                    .map(|a| ArticleView::node(a.clone()).into())
                    .collect()
            })
            .unwrap_or_else(|| vec![el("p").child("Loading articles...").into()]);

        el("section")
            .attr("class", "mb-3")
            .children(vec![
                el("h2").child("Articles").into(),
                el("p")
                    .children(vec![
                        "These articles have been pulled in from ".into(),
                        el("a")
                            .attr("href", "http://realworld.io")
                            .child("realworld.io")
                            .into(),
                        ".".into(),
                    ])
                    .into(),
                el("hr").into(),
            ])
            .children(articles)
            .into()
    }
}

pub struct ArticleView {
    article: Article,
}

impl Component for ArticleView {
    type Props = Article;

    fn new(article: Article) -> Self {
        Self { article }
    }

    fn render(&self) -> RenderNode {
        let badges = self
            .article
            .tag_list
            .iter()
            .map(|tag| {
                el("span")
                    .attr("class", "badge badge-secondary ml-2")
                    .child(tag)
                    .into()
            })
            .collect();

        el("article")
            .attr("class", "mb-5")
            .children(vec![
                el("h3")
                    .attr("class", "mb-0")
                    .child(&self.article.title)
                    .into(),
                el("p")
                    .attr("class", "d-flex justify-content-between")
                    .children(vec![
                        el("span")
                            .attr("class", "text-muted")
                            .child("By: ")
                            .child(&self.article.author.username)
                            .into(),
                        el("span").children(badges).into(),
                    ])
                    .into(),
                el("div").markdown(&self.article.body).into(),
            ])
            .into()
    }
}
