#![deny(bare_trait_objects)]
#![warn(clippy::all)]

mod articles;
mod todo;

use {
    blast::{
        render::{el, RenderComponent, ComponentExt, RenderNode},
        Component, Registry,
    },
    blast_app::Init,
};

use crate::{
    articles::{ArticleList, ArticleView},
    todo::TodoList,
};

pub struct AppInit;

impl Init for AppInit {
    fn init(&self) -> (Registry, RenderComponent) {
        let mut registry = Registry::new();

        registry.add::<AppRoot>();
        registry.add::<HelloWorld>();
        registry.add::<ArticleList>();
        registry.add::<ArticleView>();
        registry.add::<TodoList>();

        (registry, AppRoot::node(()))
    }
}

struct AppRoot;

impl Component for AppRoot {
    type Props = ();

    fn new(_: ()) -> Self {
        Self
    }

    fn render(&self) -> RenderNode {
        el("section")
            .attr("class", "container-fluid")
            .children(vec![
                el("h1").child("Complex Example").into(),
                el("p").child("This is a complex example in Blast.").into(),
                HelloWorld::node("Properties".to_string()).into(),
                TodoList::node(()).into(),
                ArticleList::node(()).into(),
            ])
            .into()
    }
}

struct HelloWorld {
    text: String,
}

impl Component for HelloWorld {
    type Props = String;

    fn new(props: String) -> Self {
        Self { text: props }
    }

    fn render(&self) -> RenderNode {
        el("section")
            .attr("class", "mb-3")
            .children(vec![
                el("h2").child("Hello Component").into(),
                el("p").child("Hello from ").child(&self.text).into(),
            ])
            .into()
    }
}
