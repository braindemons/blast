use blast::{
    event::{Context, Event},
    render::{el, RenderNode},
    Component,
};

struct Todo {
    description: String,
}

pub struct TodoList {
    entries: Vec<Todo>,
}

impl Component for TodoList {
    type Props = ();

    fn new(_: ()) -> Self {
        TodoList {
            entries: vec![Todo {
                description: "This is an example to-do entry".to_string(),
            }],
        }
    }

    fn render(&self) -> RenderNode {
        let form = el("form")
            .children(vec![
                el("div")
                    .attr("class", "form-group")
                    .children(vec![
                        el("label").child("Reminder Description").into(),
                        el("input").attr("class", "form-control").into(),
                    ])
                    .into(),
                el("button")
                    .attr("type", "submit")
                    .attr("class", "btn btn-primary")
                    .on(Event::Click, on_submit)
                    .child("Add")
                    .into(),
            ])
            .into();

        let entries = self
            .entries
            .iter()
            .map(|entry| {
                el("li")
                    .child(
                        el("div")
                            .attr("class", "d-flex justify-content-between")
                            .children(vec![
                                el("span").child(&entry.description).into(),
                                el("button")
                                    .attr("class", "btn btn-danger")
                                    .child("X")
                                    .into(),
                            ]),
                    )
                    .into()
            })
            .collect();

        el("section")
            .attr("class", "mb-3")
            .children(vec![
                el("h2").child("Todo").into(),
                form,
                el("hr").into(),
                el("ul").children(entries).into(),
            ])
            .into()
    }
}

fn on_submit(ctx: Context<TodoList>) {
    let todo = Todo {
        description: "Placeholder description".to_string(),
    };
    ctx.set(|c| c.entries.push(todo));
}