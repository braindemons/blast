#![deny(bare_trait_objects)]
#![warn(clippy::all)]

fn main() {
    // Static file serving in this is included to make development easier.
    // In a production environment you should use a more optimized web server like nginx, or a CDN.

    let template = include_str!("../data/template.html");

    let socket_addr = "127.0.0.1:3000".parse().unwrap();
    let init = demo_app::AppInit;
    blast_app_server::run(socket_addr, init, template);
}
