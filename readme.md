# Blast

Blast is an *experimental* isomorphic web frontend framework for Rust.


## Features

- A familiar React-inspired declarative rendering API
- Fairly small gzipped (examples/demo is 170 KB gzipped)
- vDOM diffing for minimal DOM changes
- Optional futures in events for asynchronous data fetching
- Server-side pre-rendering with client-side hydration


## Experimental

This framework is extremely early in development and experimental.
Do not use it in production, unless you can take the risk and make the improvements you need
yourself.


## Run Example

```bash
cd examples/demo/client
yarn run build
cd ..
cargo run --bin demo-server
```


## License
Licensed under either of
 * Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT License (Expat) ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.
