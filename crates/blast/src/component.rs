use std::any::Any;

use downcast_rs::{impl_downcast, Downcast};

use crate::{render::RenderNode, Blast, ComponentId, event::Context};

pub type FetchHandler = Box<dyn FnOnce(Blast)>;

pub trait Component: Any + Sized {
    type Props;

    // TODO: Props changed event

    fn new(props: Self::Props) -> Self;

    /// Component lifetime event.
    /// This event lets you dispatch async data fetches.
    fn on_fetch(_ctx: Context<Self>) {}

    fn render(&self) -> RenderNode;
}

pub trait DynComponent: Downcast {
    fn dyn_render(&self) -> RenderNode;
    fn dyn_create_fetch_handler(&self, id: ComponentId) -> FetchHandler;
}

impl_downcast!(DynComponent);

impl<C: Component> DynComponent for C {
    fn dyn_render(&self) -> RenderNode {
        self.render()
    }

    fn dyn_create_fetch_handler(&self, id: ComponentId) -> FetchHandler {
        Box::new(move |blast| {
            let ctx = Context::new(blast, id);
            C::on_fetch(ctx)
        })
    }
}
