use std::collections::VecDeque;

use slotmap::{new_key_type, SecondaryMap, SlotMap};

use crate::{
    component::{DynComponent, FetchHandler},
    diff::{Diff, ComponentAdded},
    dom::ActiveNode,
    render::RenderComponent,
    patch::patch_component,
    Registry,
};

new_key_type! { pub struct ComponentId; }

pub type ComponentIds = SlotMap<ComponentId, ()>;

pub struct Components {
    registry: Registry,
    ids: ComponentIds,
    entries: SecondaryMap<ComponentId, ComponentEntry>,
}

impl Components {
    pub fn new(registry: Registry) -> Self {
        Self {
            registry,
            ids: SlotMap::with_key(),
            entries: SecondaryMap::new(),
        }
    }

    pub fn component_ref(&self, id: ComponentId) -> Option<&dyn DynComponent> {
        self.entries.get(id).map(|entry| entry.component.as_ref())
    }

    pub fn component_mut(&mut self, id: ComponentId) -> Option<&mut dyn DynComponent> {
        self.entries
            .get_mut(id)
            .map(|entry| entry.component.as_mut())
    }

    pub fn component_vdom(&self, id: ComponentId) -> Option<ActiveNode> {
        self.entries.get(id).map(|c| c.vdom.clone())
    }

    pub fn fetch_handlers_for_components(&self, targets: &[ComponentId]) -> Vec<FetchHandler> {
        let mut fetch_handlers = Vec::new();

        for target in targets {
            let fetch_handler = self
                .entries
                .get(*target)
                .unwrap()
                .component
                .dyn_create_fetch_handler(*target);

            fetch_handlers.push(fetch_handler);
        }

        fetch_handlers
    }

    pub fn add_component(&mut self, vcomp: RenderComponent) -> (ComponentId, Diff) {
        let mut queue = VecDeque::new();
        let mut diff = Diff::new();

        // Add the root
        let id = self.ids.insert(());
        self.add_component_shallow(id, vcomp, &mut queue, &mut diff);

        // Add children until they're all resolved
        while let Some((id, vcomp)) = queue.pop_front() {
            self.add_component_shallow(id, vcomp, &mut queue, &mut diff);
        }

        (id, diff)
    }

    /// Update the DOM starting at the given component.
    pub fn update_dom(&mut self, target: ComponentId) -> Diff {
        let mut diff = Diff::new();

        // Re-render the component
        let entry = self.entries.get_mut(target).unwrap();
        let vdom = entry.component.dyn_render();

        // Find differences between the previous dom and the new dom, and patch it with that
        let (component_diff, new_components, _) =
            patch_component(&mut entry.vdom, vdom, target, &mut self.ids);
        diff.changed.push(component_diff);

        // TODO: Submit new properties to already existing child components
        // TODO: Update the DOM of child components if they tell us to on new properties

        // Create the queue we'll resolve children from
        let mut queue = VecDeque::new();
        queue.extend(new_components);

        // Add children until they're all resolved
        while let Some((id, vcomp)) = queue.pop_front() {
            self.add_component_shallow(id, vcomp, &mut queue, &mut diff);
        }

        diff
    }

    fn add_component_shallow(
        &mut self,
        id: ComponentId,
        vcomp: RenderComponent,
        queue: &mut VecDeque<(ComponentId, RenderComponent)>,
        diff: &mut Diff,
    ) -> ComponentId {
        // Create the root instance
        let component = self.registry.create(vcomp.type_id, vcomp.props);

        // Render the new instance
        let vdom = component.dyn_render();

        // Diff and patch the component against an empty node, we won't use the diff because this is
        // a brand new component.
        // TODO: This can just be a call to patch_new_tree
        let mut active_vdom = ActiveNode::Text("".to_string());
        let (_, new_components, _) = patch_component(&mut active_vdom, vdom, id, &mut self.ids);

        // Add the new children to the queue
        queue.extend(new_components);

        // Record the new component added in the diff
        diff.added.push(ComponentAdded::new(id));

        // Insert the component
        self.entries.insert(
            id,
            ComponentEntry {
                component,
                vdom: active_vdom,
            },
        );

        id
    }
}

struct ComponentEntry {
    component: Box<dyn DynComponent>,
    vdom: ActiveNode,
}
