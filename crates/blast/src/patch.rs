use std::collections::HashMap;

use crate::{
    components::ComponentIds,
    diff::{ChildrenAppended, ComponentChanged, NodeReplaced, Position, TextChanged},
    dom::{ActiveElement, ActiveNode, ElementContent},
    event::{AnyHandler, Event},
    render::{RenderComponent, RenderElement, RenderNode},
    ComponentId,
};

type QueuedNewComponent = (ComponentId, RenderComponent);
type ExtractedEvent = (Event, Position, AnyHandler);

struct PatchContext<'a> {
    changed: &'a mut ComponentChanged,
    component_ids: &'a mut ComponentIds,
    new_components: &'a mut Vec<QueuedNewComponent>,
    events: &'a mut Vec<ExtractedEvent>,
}

/// Compares an Active DOM to a new Render DOM, finding differences and patching them into the
/// active DOM.
/// Pre-allocates IDs for new components into the component_ids slotmap.
/// Returns the patched differences as a Diff.
/// Returns a list of new components that need to be initialized.
/// Returns a list of events extracted from the new Render DOM.
pub fn patch_component(
    target: &mut ActiveNode,
    source: RenderNode,
    component: ComponentId,
    component_ids: &mut ComponentIds,
) -> (
    ComponentChanged,
    Vec<QueuedNewComponent>,
    Vec<ExtractedEvent>,
) {
    let mut changed = ComponentChanged::new(component);
    let mut new_components = Vec::new();
    let mut events = Vec::new();

    let position = Vec::new();

    patch_node(
        target,
        source,
        &position,
        &mut PatchContext {
            changed: &mut changed,
            component_ids,
            new_components: &mut new_components,
            events: &mut events,
        },
    );

    (changed, new_components, events)
}

fn patch_node(
    target: &mut ActiveNode,
    source: RenderNode,
    position: &[usize],
    context: &mut PatchContext,
) {
    match source {
        RenderNode::Element(source_element) => {
            patch_node_with_element(target, source_element, position, context);
        }
        RenderNode::Component(component) => {
            // TODO: Implement diffing, this currently just always re-creates the component.
            // Not even the old component is cleaned up here.
            apply_replace_patch(target, RenderNode::Component(component), position, context);
        }
        RenderNode::Text(text) => {
            // If our source and target match in type
            if let ActiveNode::Text(target_text) = target {
                // Verify that the text is correct
                if text != *target_text {
                    // It's not correct, apply and add a diff
                    *target_text = text.clone();
                    context
                        .changed
                        .text_changed
                        .push(TextChanged { new_text: text });
                }
            } else {
                // It's an entirely different node type, change the target node and add a diff
                apply_replace_patch(target, RenderNode::Text(text), position, context);
            }
        }
    }
}

fn patch_node_with_element(
    target: &mut ActiveNode,
    source_element: RenderElement,
    position: &[usize],
    context: &mut PatchContext,
) {
    // If our source and target match in type
    if let ActiveNode::Element(target_element) = target {
        // Verify that we've got the same tag
        if source_element.tag == target_element.tag {
            // Check if we can patch the content at all, or if it needs a full replacement
            let needs_replacement =
                check_if_content_needs_replacement(&target_element, &source_element);

            // If it doesn't need replacement we can keep patching it
            if !needs_replacement {
                // TODO: Diff attributes

                // Add events for this element
                record_events(source_element.events, position, context);

                // Patch the content we previously verified we can patch
                patch_content(target_element, source_element.content, position, context);

                // We don't need to replace later
                return;
            }
        }
    }

    // It's a different node type, a different tag, or the content can't be patched, so we need to
    // replace it entirely
    apply_replace_patch(
        target,
        RenderNode::Element(source_element),
        position,
        context,
    );
}

fn check_if_content_needs_replacement(
    target_element: &ActiveElement,
    source_element: &RenderElement,
) -> bool {
    match &source_element.content {
        ElementContent::Children(_) => {
            if let ElementContent::Children(_) = &target_element.content {
                // We can patch this
                return false;
            }
        }
        ElementContent::RawHtml(raw_html) => {
            if let ElementContent::RawHtml(target_raw_html) = &target_element.content {
                if *raw_html == *target_raw_html {
                    // It matches
                    return false;
                }
            }
        }
        ElementContent::Empty => {
            if target_element.content.is_empty() {
                // It matches
                return false;
            }
        }
    }

    // It doesn't match and we can't patch it, so it needs replacement
    true
}

fn patch_content(
    target_element: &mut ActiveElement,
    source_content: ElementContent<RenderNode>,
    position: &[usize],
    context: &mut PatchContext,
) {
    if let ElementContent::Children(source_children) = source_content {
        if let ElementContent::Children(target_children) = &mut target_element.content {
            patch_children(target_children, source_children, position, context);
        }
    }

    // Anything else we previously verified as matching in check_if_content_needs_replacement
}

fn patch_children(
    target_children: &mut Vec<ActiveNode>,
    mut source_children: Vec<RenderNode>,
    position: &[usize],
    context: &mut PatchContext,
) {
    // Go through the children and match them
    let min = target_children.len().min(source_children.len());

    // We need to take the values out of the source, so use drain
    for (i, source_child) in source_children.drain(0..min).enumerate() {
        let target_child = target_children.get_mut(i).unwrap();

        // Diff these nodes individually
        let mut child_position = position.to_vec();
        child_position.push(i);
        patch_node(target_child, source_child, &child_position, context);
    }

    // If we've still got source elements left, patch those in and add a diff
    if !source_children.is_empty() {
        let new_children: Vec<_> = source_children
            .into_iter()
            .enumerate()
            .map(|(i, source_child)| {
                let mut child_position = position.to_vec();
                child_position.push(min + i);
                patch_new_tree(source_child, &child_position, context)
            })
            .collect();

        // Add the new children
        for new_child in &new_children {
            target_children.push(new_child.clone());
        }

        // Record a diff
        context.changed.children_appended.push(ChildrenAppended {
            new_children,
            position: position.to_vec(),
        });
    }

    // TODO: Remove excess children and record a diff
}

fn apply_replace_patch(
    target: &mut ActiveNode,
    source: RenderNode,
    position: &[usize],
    context: &mut PatchContext,
) {
    *target = patch_new_tree(source, position, context);
    context.changed.nodes_replaced.push(NodeReplaced {
        new_node: target.clone(),
        position: position.to_vec(),
    });
}

fn patch_new_tree(
    source: RenderNode,
    position: &[usize],
    context: &mut PatchContext,
) -> ActiveNode {
    match source {
        RenderNode::Element(source_element) => {
            let element = patch_new_element(source_element, position, context);
            ActiveNode::Element(element)
        }
        RenderNode::Component(vcomp) => {
            // Reserve an ID for this component
            let id = context.component_ids.insert(());

            // Add the component to the new components that need to be created
            context.new_components.push((id, vcomp));

            // Replace the node with an active component
            ActiveNode::Component(id)
        }
        RenderNode::Text(text) => ActiveNode::Text(text.clone()),
    }
}

fn patch_new_element(
    element: RenderElement,
    position: &[usize],
    context: &mut PatchContext,
) -> ActiveElement {
    record_events(element.events, position, context);

    let content = match element.content {
        ElementContent::Children(children) => {
            let children = children
                .into_iter()
                .enumerate()
                .map(|(i, child)| {
                    let mut child_position = position.to_vec();
                    child_position.push(i);
                    patch_new_tree(child, &child_position, context)
                })
                .collect();
            ElementContent::Children(children)
        }
        ElementContent::RawHtml(html) => ElementContent::RawHtml(html),
        ElementContent::Empty => ElementContent::Empty,
    };

    ActiveElement {
        tag: element.tag,
        attributes: element.attributes,
        content,
    }
}

fn record_events(
    events: HashMap<Event, AnyHandler>,
    position: &[usize],
    context: &mut PatchContext,
) {
    for (event, handler) in events {
        context.events.push((event, position.to_vec(), handler));
    }
}