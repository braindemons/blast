use std::any::{TypeId, Any};

use crate::Component;

#[derive(Debug)]
pub struct RenderComponent {
    pub type_id: TypeId,
    pub props: Box<dyn Any>,
}

pub trait ComponentExt<C: Component> {
    fn node(props: C::Props) -> RenderComponent;
}

impl<C: Component> ComponentExt<C> for C {
    fn node(props: C::Props) -> RenderComponent {
        RenderComponent {
            type_id: TypeId::of::<C>(),
            props: Box::new(Some(props)),
        }
    }
}
