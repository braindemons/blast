use std::{collections::HashMap};

use crate::{
    dom::ElementContent,
    render::RenderNode,
    event::{Event, Handler, AnyHandler},
};

pub struct RenderElement {
    pub tag: String,
    pub attributes: HashMap<String, String>,
    pub events: HashMap<Event, AnyHandler>,
    pub content: ElementContent<RenderNode>,
}

impl RenderElement {
    pub fn new<S: ToString>(tag: S) -> Self {
        Self {
            tag: tag.to_string(),
            attributes: HashMap::new(),
            events: HashMap::new(),
            content: ElementContent::Empty,
        }
    }

    pub fn attr<K: ToString, V: ToString>(mut self, key: K, value: V) -> Self {
        let key = key.to_string();
        let value = value.to_string();

        self.attributes.insert(key, value);

        self
    }

    pub fn on<C, H: Handler<C> + 'static>(mut self, event: Event, handler: H) -> Self {
        self.events.insert(event, AnyHandler::from_handler(handler));
        self
    }

    pub fn children(mut self, children: Vec<RenderNode>) -> Self {
        let content = self.content.enforce_children();
        content.extend(children);
        self
    }

    pub fn child<N: Into<RenderNode>>(mut self, child: N) -> Self {
        let content = self.content.enforce_children();
        content.push(child.into());
        self
    }

    pub fn raw_html<S: ToString>(mut self, html: S) -> Self {
        self.content = ElementContent::RawHtml(html.to_string());
        self
    }
}
