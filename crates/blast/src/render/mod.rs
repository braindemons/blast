mod component;
mod element;

pub use self::{component::{RenderComponent, ComponentExt}, element::RenderElement};

pub enum RenderNode {
    Element(RenderElement),
    Component(RenderComponent),
    Text(String),
}

impl From<RenderElement> for RenderNode {
    fn from(node: RenderElement) -> Self {
        RenderNode::Element(node)
    }
}

impl From<RenderComponent> for RenderNode {
    fn from(node: RenderComponent) -> Self {
        RenderNode::Component(node)
    }
}

impl<'a> From<&'a str> for RenderNode {
    fn from(text: &'a str) -> Self {
        RenderNode::Text(text.into())
    }
}

impl From<String> for RenderNode {
    fn from(text: String) -> Self {
        RenderNode::Text(text)
    }
}

impl<'a> From<&'a String> for RenderNode {
    fn from(text: &'a String) -> Self {
        RenderNode::Text(text.clone())
    }
}

pub fn el<S: ToString>(tag: S) -> RenderElement {
    RenderElement::new(tag)
}
