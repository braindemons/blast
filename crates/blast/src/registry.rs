use std::{
    any::{Any, TypeId},
    collections::HashMap,
};

use crate::{Component, DynComponent};

type FactoryFn = dyn Fn(Box<dyn Any>) -> Box<dyn DynComponent>;

pub struct Registry {
    factories: HashMap<TypeId, Box<FactoryFn>>,
}

impl Registry {
    #[allow(clippy::new_without_default)]
    pub fn new() -> Self {
        Self {
            factories: HashMap::new(),
        }
    }

    pub fn add<C: Component>(&mut self) {
        let factory = |mut props_any: Box<dyn Any>| {
            let props: &mut Option<C::Props> = props_any.downcast_mut().unwrap();
            let component = C::new(props.take().unwrap());
            Box::new(component) as Box<dyn DynComponent>
        };

        self.factories.insert(TypeId::of::<C>(), Box::new(factory));
    }

    pub(crate) fn create(&self, type_id: TypeId, props: Box<dyn Any>) -> Box<dyn DynComponent> {
        // TODO: Add Result error
        self.factories
            .get(&type_id)
            .map(|f| f(props))
            .expect("Requested component type doesn't exist in registry")
    }
}
