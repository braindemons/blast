use crate::{dom::ActiveNode, ComponentId};

#[derive(Default, Debug)]
pub struct Diff {
    pub added: Vec<ComponentAdded>,
    pub changed: Vec<ComponentChanged>,
}

impl Diff {
    pub fn new() -> Self {
        Self {
            added: Vec::new(),
            changed: Vec::new(),
        }
    }
}

#[derive(Debug)]
pub struct ComponentAdded {
    pub target: ComponentId,
}

impl ComponentAdded {
    pub fn new(target: ComponentId) -> Self {
        Self { target }
    }
}

#[derive(Debug)]
pub struct ComponentChanged {
    pub target: ComponentId,
    pub text_changed: Vec<TextChanged>,
    pub nodes_replaced: Vec<NodeReplaced>,
    pub children_appended: Vec<ChildrenAppended>,
}

impl ComponentChanged {
    pub fn new(target: ComponentId) -> Self {
        Self {
            target,
            text_changed: Vec::new(),
            nodes_replaced: Vec::new(),
            children_appended: Vec::new(),
        }
    }
}

/// A chain of DOM indices to find the child relative to a parent.
pub type Position = Vec<usize>;

#[derive(Debug)]
pub struct TextChanged {
    pub new_text: String,
}

#[derive(Debug)]
pub struct NodeReplaced {
    pub new_node: ActiveNode,
    pub position: Position,
}

#[derive(Debug)]
pub struct ChildrenAppended {
    pub new_children: Vec<ActiveNode>,
    pub position: Position,
}
