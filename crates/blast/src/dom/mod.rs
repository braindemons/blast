use std::collections::HashMap;

use crate::ComponentId;

#[derive(Debug, Clone)]
pub enum ActiveNode {
    Element(ActiveElement),
    Component(ComponentId),
    Text(String),
}

#[derive(Debug, Clone)]
pub struct ActiveElement {
    pub tag: String,
    pub attributes: HashMap<String, String>,
    pub content: ElementContent<ActiveNode>,
}

#[derive(Debug, Clone)]
pub enum ElementContent<C> {
    Children(Vec<C>),
    RawHtml(String),
    Empty,
}

impl<C> ElementContent<C> {
    pub fn is_children(&self) -> bool {
        if let ElementContent::Children(_) = self {
            true
        } else {
            false
        }
    }

    pub fn is_raw_html(&self) -> bool {
        if let ElementContent::RawHtml(_) = self {
            true
        } else {
            false
        }
    }

    pub fn is_empty(&self) -> bool {
        if let ElementContent::Empty = self {
            true
        } else {
            false
        }
    }

    fn children_mut(&mut self) -> Option<&mut Vec<C>> {
        if let ElementContent::Children(children) = self {
            Some(children)
        } else {
            None
        }
    }

    pub(crate) fn enforce_children(&mut self) -> &mut Vec<C> {
        if self.is_children() {
            self.children_mut().unwrap()
        } else {
            *self = ElementContent::Children(Vec::new());
            self.children_mut().unwrap()
        }
    }
}
