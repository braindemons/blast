use std::{cell::RefCell, rc::Rc};

use crate::{
    components::Components,
    diff::Diff,
    dom::ActiveNode,
    render::RenderComponent,
    ComponentId, PlatformHandler, Registry, UnitFuture,
};

#[derive(Clone)]
pub struct Blast {
    components: Rc<RefCell<Components>>,
    platform_handler: Rc<dyn PlatformHandler>,
}

impl Blast {
    pub fn new<H: PlatformHandler>(registry: Registry, platform_handler: H) -> Self {
        Self {
            components: Rc::new(RefCell::new(Components::new(registry))),
            platform_handler: Rc::new(platform_handler),
        }
    }

    pub(crate) fn components(&self) -> &RefCell<Components> {
        &self.components
    }

    pub fn component_vdom(&self, id: ComponentId) -> Option<ActiveNode> {
        self.components.borrow().component_vdom(id)
    }

    /// Add a component instance to this Blast instance.
    pub fn add_component(&self, vcomp: RenderComponent) -> ComponentId {
        // Add the component
        let (id, diff) = self.components.borrow_mut().add_component(vcomp);

        // Run the fetch handlers
        self.run_fetch_handlers_for_diff(&diff);

        id
    }

    pub(crate) fn run_fetch_handlers_for_diff(&self, diff: &Diff) {
        // Be careful with the reference guards here, don't leave anything borrowed while running
        // handlers

        // Retrieve and the on_fetch events
        let components: Vec<_> = diff.added.iter().map(|new| new.target).collect();
        let fetch_handlers = self
            .components
            .borrow()
            .fetch_handlers_for_components(&components);

        // Run the fetch events
        for fetch_handler in fetch_handlers {
            fetch_handler(self.clone());
        }
    }

    pub(crate) fn submit_future(&self, future: UnitFuture) {
        self.platform_handler.handle_future(future);
    }

    pub(crate) fn submit_diff(&self, diff: &Diff) {
        self.platform_handler.handle_diff(self, diff);
    }
}
