#![deny(bare_trait_objects)]
#![warn(clippy::all)]

mod blast;
mod component;
mod components;
pub mod diff;
pub mod dom;
pub mod event;
mod patch;
mod registry;
pub mod render;

pub use self::{blast::Blast, component::Component, components::ComponentId, registry::Registry};

use futures::Future;

use crate::{component::DynComponent, diff::Diff};

pub type UnitFuture = Box<dyn Future<Item = (), Error = ()>>;

pub trait PlatformHandler: 'static {
    fn handle_future(&self, future: UnitFuture);
    fn handle_diff(&self, blast: &Blast, diff: &Diff);
}
