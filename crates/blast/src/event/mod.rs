mod context;

pub use self::context::Context;

use std::any::Any;

#[derive(Clone, PartialEq, Eq, Hash, Debug)]
pub enum Event {
    Click
}

pub trait Handler<C> {
    fn handle(&mut self, ctx: Context<C>);
}

impl<C, F: FnMut(Context<C>)> Handler<C> for F {
    fn handle(&mut self, ctx: Context<C>) {
        (self)(ctx)
    }
}

pub struct AnyHandler {
    handler: Box<dyn Any>,
}

impl AnyHandler {
    pub fn from_handler<C, H: Handler<C> + 'static>(handler: H) -> Self {
        Self {
            handler: Box::new(handler)
        }
    }
}