use std::marker::PhantomData;

use futures::Future;

use crate::{Blast, Component, ComponentId};

/// A context through which an event handler can apply changes to the state of components.
pub struct Context<C> {
    blast: Blast,
    target: ComponentId,
    _c: PhantomData<C>,
}

impl<C: Component> Context<C> {
    pub(crate) fn new(blast: Blast, target: ComponentId) -> Self {
        Self {
            blast,
            target,
            _c: PhantomData::default(),
        }
    }

    // TODO: A component may no longer be accessable when the accessor functions on this are called.
    // Hanlde that case.

    /// Access the component immutably, does not trigger a re-render.
    pub fn get<O, F: FnOnce(&C) -> O>(&self, accessor: F) -> O {
        let components = self.blast.components().borrow();

        // Get the component we want to access
        // TODO: The component may no longer exist
        let component = components.component_ref(self.target).unwrap();
        let component: &C = component.downcast_ref().unwrap();

        // Run the accessor
        accessor(component)
    }

    /// Change the state of the component, triggering a re-render.
    pub fn set<F: FnOnce(&mut C)>(&self, accessor: F) {
        let mut components = self.blast.components().borrow_mut();

        // Get the component we want to apply to
        // TODO: The component may no longer exist
        let component = components.component_mut(self.target).unwrap();
        let component: &mut C = component.downcast_mut().unwrap();

        // Apply the change
        accessor(component);

        // Update the DOM starting at the target now that it has changed
        let diff = components.update_dom(self.target);

        // Drop the reference guard before we start running the fetch handlers to avoid crashes
        drop(components);

        // Submit the diff so the real DOM can be updated
        self.blast.submit_diff(&diff);

        // Run fetch handlers. This is done after submitting the diff, because this can cause more
        // DOM changes, and thus newer diffs to be submitted.
        self.blast.run_fetch_handlers_for_diff(&diff);
    }

    /// Dispatches a future to be handled by the async runtime.
    pub fn dispatch<F: Future<Item = (), Error = ()> + 'static>(&self, future: F) {
        self.blast.submit_future(Box::new(future));
    }
}

impl<C> Clone for Context<C> {
    fn clone(&self) -> Self {
        Self {
            blast: self.blast.clone(),
            target: self.target,
            _c: PhantomData::default(),
        }
    }
}
