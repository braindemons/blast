#![deny(bare_trait_objects)]
#![warn(clippy::all)]

use blast::{Registry, render::RenderComponent};

pub trait Init: Send + Sync + 'static {
    fn init(&self) -> (Registry, RenderComponent);
}