#![deny(bare_trait_objects)]
#![warn(clippy::all)]

#[cfg(not(target_arch = "wasm32"))]
mod server {
    use {
        futures::{Future, Stream},
        hyper::{Body, Client, Request},
        serde::de::DeserializeOwned,
    };

    pub fn fetch<D: DeserializeOwned>(uri: &str) -> impl Future<Item = D, Error = ()> {
        let client = Client::new();

        let req = Request::builder()
            .method("GET")
            .uri(uri)
            .body(Body::empty())
            .unwrap();

        let future = client.request(req);

        future
            .and_then(|res| res.into_body().concat2())
            .map(|body| {
                // TODO: Deserialization errors
                let data: D = serde_json::from_slice(&body).unwrap();
                data
            })
            .map_err(|error| {
                // TODO: Improve error handling
                println!("Fetch failed: {}", error);
            })
    }
}

#[cfg(not(target_arch = "wasm32"))]
pub use server::*;

#[cfg(target_arch = "wasm32")]
mod client {
    use {
        futures::{future::{self, Either}, Future},
        serde::de::DeserializeOwned,
        wasm_bindgen::JsCast,
        wasm_bindgen_futures::JsFuture,
        web_sys::{Request, RequestInit, Response},
        js_sys::{ArrayBuffer, Uint8Array},
    };

    pub fn fetch<D: DeserializeOwned>(uri: &str) -> impl Future<Item = D, Error = ()> {
        let window = web_sys::window().unwrap();

        // Configure the request we want to fetch
        let mut opts = RequestInit::new();
        opts.method("GET");
        let request = Request::new_with_str_and_init(uri, &opts).unwrap();

        // Initiate the fetch
        let request_promise = window.fetch_with_request(&request);

        // Turn the fetch into a rust future
        JsFuture::from(request_promise)
            // TODO: Improve error handling
            .map_err(|_| ())
            .and_then(|response| {
                // Get the response out of the JsValue
                let response: Response = response.dyn_into().unwrap();

                // Check for errors
                let status = response.status();
                if status >= 400 {
                    return Either::A(future::err(()))
                }

                // Extract the data
                let promise = response.array_buffer().unwrap();
                Either::B(JsFuture::from(promise).map_err(|_| ()))
            })
            .map(|array_buffer| {
                // Get a JS u8 array out of the array buffer
                let buffer: ArrayBuffer = array_buffer.dyn_into().unwrap();
                let array = Uint8Array::new(&buffer);

                // Copy the data to Rust memory
                let mut data = vec![0; array.byte_length() as usize];
                array.copy_to(&mut data);

                // Deserialize the data
                // TODO: Deserialization errors
                let data: D = serde_json::from_slice(&data).unwrap();
                data
            })
    }
}

#[cfg(target_arch = "wasm32")]
pub use client::*;

/*
        let window = web_sys::window().unwrap();

        // Set up and run the request for the data
        let mut opts = RequestInit::new();
        opts.method("GET");

        let url = format!("data/{}", file_name);
        let request = Request::new_with_str_and_init(&url, &opts).unwrap();

        let request_promise = window.fetch_with_request(&request);

        // Handle the response
        let future = JsFuture::from(request_promise)
            .then(|response| {
                let response = if let Ok(response) = response {
                    response
                } else {
                    return Either::A(future::err(FetchError::FetchFailed));
                };

                // Get the response out of the JsValue
                let response: Response = response.dyn_into().unwrap();

                let status = response.status();
                if status >= 400 {
                    return Either::A(future::err(FetchError::BadStatusCode(status)));
                }

                let promise = response.array_buffer().unwrap();

                let future = JsFuture::from(promise)
                    .and_then(move |array_buffer| {
                        let buffer: ArrayBuffer = array_buffer.dyn_into().unwrap();
                        let array = Uint8Array::new(&buffer);

                        let mut data = vec![0; array.byte_length() as usize];
                        array.copy_to(&mut data);

                        // Send the data back to the caller
                        success(&data);

                        future::ok(JsValue::UNDEFINED)
                    })
                    .map_err(|_| FetchError::Unexpected);

                Either::B(future)
            })
            .or_else(move |error| {
                // Perhaps we shouldn't error here and just let the caller log it?
                error!("Error while fetching data from \"{}\": {:?}", url, error);

                failure();

                // We don't want an error to be thrown for this, we do our own logging
                future::ok(JsValue::UNDEFINED)
            });
        let _promise = future_to_promise(future);
*/
