#![deny(bare_trait_objects)]
#![warn(clippy::all)]

use v_htmlescape::escape;

use blast::{
    dom::{ActiveElement, ActiveNode, ElementContent},
    Blast, ComponentId,
};

pub fn component_to_html(blast: &Blast, id: ComponentId) -> String {
    let mut html = String::new();

    let vdom = blast.component_vdom(id).unwrap();
    write_node(blast, &vdom, &mut html, &mut false);

    html
}

fn write_node(blast: &Blast, node: &ActiveNode, html: &mut String, prev_was_text: &mut bool) {
    match node {
        ActiveNode::Element(element) => {
            write_element(blast, element, html);
            *prev_was_text = false;
        }
        ActiveNode::Component(id) => {
            // Continue to the tree in the component
            let vdom = blast.component_vdom(*id).unwrap();

            write_node(blast, &vdom, html, prev_was_text);

            *prev_was_text = false;
        }
        ActiveNode::Text(text) => {
            // If the previous node was also a text node, insert a blank comment to separate the
            // nodes in the DOM on the client side
            if *prev_was_text {
                html.push_str("<!-- -->");
            }

            html.push_str(&escape(&text).to_string());

            *prev_was_text = true;
        }
    }
}

fn write_element(blast: &Blast, element: &ActiveElement, html: &mut String) {
    html.push('<');
    html.push_str(&element.tag);

    // Write out all the attributes
    for (key, value) in &element.attributes {
        html.push(' ');
        html.push_str(&key);
        html.push_str("=\"");
        html.push_str(&escape(&value).to_string());
        html.push('"');
    }

    if is_void_element(&element.tag) {
        html.push_str("/>");
    } else {
        html.push('>');

        // Raw html overwrites children
        match &element.content {
            ElementContent::Children(children) => {
                let mut prev_was_text = false;
                for child in children {
                    write_node(blast, child, html, &mut prev_was_text);
                }
            }
            ElementContent::RawHtml(raw_html) => {
                // This can result in broken HTML, right now we're assuming the HTML will be
                // valid. If it isn't valid it will be fixed in hydration anyways and show as
                // an error. Potentially we could add a sanitizer.
                html.push_str(raw_html);
            }
            ElementContent::Empty => {}
        }

        html.push_str("</");
        html.push_str(&element.tag);
        html.push('>');
    }
}

fn is_void_element(tag: &str) -> bool {
    match tag {
        "area" => true,
        "base" => true,
        "br" => true,
        "col" => true,
        "command" => true,
        "embed" => true,
        "hr" => true,
        "img" => true,
        "input" => true,
        "keygen" => true,
        "link" => true,
        "meta" => true,
        "param" => true,
        "source" => true,
        "track" => true,
        "wbr" => true,
        _ => false,
    }
}