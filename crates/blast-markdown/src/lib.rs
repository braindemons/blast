#![deny(bare_trait_objects)]
#![warn(clippy::all)]

use {
    v_htmlescape::escape,
    pulldown_cmark::{Parser, Options, html}
};

use blast::render::RenderElement;

pub trait MarkdownExt {
    fn markdown(self, markdown: &str) -> Self;
}

impl MarkdownExt for RenderElement {
    fn markdown(self, markdown: &str) -> Self {
        // I don't really want to bother with selectively allowing HTML in the input
        let escaped = escape(markdown).to_string();

        let parser = Parser::new_ext(&escaped, Options::empty());

        let mut html_output = String::new();
        html::push_html(&mut html_output, parser);

        self.raw_html(html_output)
    }
}