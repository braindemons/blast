use std::path::PathBuf;

use {
    actix_files::NamedFile,
    actix_web::{HttpRequest, Result},
};

pub fn static_file_handler(req: HttpRequest) -> Result<NamedFile> {
    // Assemble the path relative to the dist directory
    let relative_path: PathBuf = req.match_info().query("tail").into();

    // First try the dist directory, it has priority
    let mut path = PathBuf::from("./package/dist/");
    path.push(&relative_path);

    // If we couldn't find a file in dist, instead go to static
    if !path.exists() {
        path = PathBuf::from("./static/");
        path.push(&relative_path);
    }

    let mut named_file = NamedFile::open(path)?;

    // If this is a wasm file, give it the right MIME type
    if relative_path
        .extension()
        .map(|v| v == "wasm")
        .unwrap_or(false)
    {
        named_file = named_file.set_content_type("application/wasm".parse().unwrap());
    }

    Ok(named_file)
}
