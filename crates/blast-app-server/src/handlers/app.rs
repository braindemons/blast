use std::{cell::RefCell, path::PathBuf, rc::Rc};

use {
    actix_web::{http, HttpRequest, HttpResponse},
    tokio::prelude::{
        future::{join_all, loop_fn, Loop},
        Future,
    },
};

use {
    blast::{diff::Diff, Blast, PlatformHandler, UnitFuture},
    blast_app::Init,
    blast_html::component_to_html,
};

pub fn app_handler<I: Init>(
    req: HttpRequest,
    init: &I,
    template: &str,
) -> impl Future<Item = HttpResponse, Error = ()> {
    let _relative_path: PathBuf = req.match_info().query("tail").into();

    // Initialize the app's data
    let (registry, root) = init.init();

    // Create the future handler, which we'll use to resolve futures until the app's idle
    let pending_futures = Rc::new(RefCell::new(Vec::new()));
    let future_handler = ServerHandler {
        pending_futures: pending_futures.clone(),
    };

    // Create a new Blast instance and add the app to it
    let blast = Blast::new(registry, future_handler);
    let id = blast.add_component(root);

    let template = template.to_string();
    loop_fn((blast, pending_futures), move |(blast, pending_futures)| {
        let template = template.clone();

        // Resolve all pending futures
        join_all(take_all_futures(&pending_futures)).and_then(move |_| {
            // Applies can cause re-renders, which can cause new components to spawn new
            // futures, so loop if we've got more futures left
            if !pending_futures.borrow().is_empty() {
                Ok(Loop::Continue((blast, pending_futures)))
            } else {
                // Render the vDOM to HTML
                let html = component_to_html(&blast, id);

                // Wrap the app in a container
                let mut app_html = String::new();
                app_html.push_str("<div id=\"app\">");
                app_html.push_str(&html);
                app_html.push_str("</div>");

                // Render the finale HTML using the template
                let final_html = template.replace("%APP%", &app_html);

                Ok(Loop::Break(
                    HttpResponse::Ok()
                        .header(http::header::CONTENT_TYPE, "text/html")
                        .body(final_html),
                ))
            }
        })
    })
}

fn take_all_futures(pending_futures: &Rc<RefCell<Vec<UnitFuture>>>) -> Vec<UnitFuture> {
    let mut pending_futures = pending_futures.borrow_mut();
    std::mem::replace(&mut pending_futures, Vec::new())
}

struct ServerHandler {
    pending_futures: Rc<RefCell<Vec<UnitFuture>>>,
}

impl PlatformHandler for ServerHandler {
    fn handle_future(&self, future: UnitFuture) {
        self.pending_futures.borrow_mut().push(future);
    }

    fn handle_diff(&self, _blast: &Blast, _diff: &Diff) {}
}
