#![deny(bare_trait_objects)]
#![warn(clippy::all)]

mod handlers;

use std::{net::SocketAddr, sync::Arc};

use actix_web::{web, App, HttpServer};

use blast_app::Init;

pub fn run<I: Init, S: ToString>(address: SocketAddr, init: I, template: S) {
    println!("Starting Server on http://{}", address);

    let init = Arc::new(init);
    let template = template.to_string();

    let factory = move || {
        let init = init.clone();
        let template = template.clone();
        let app_handler = move |req| handlers::app_handler(req, init.as_ref(), &template);

        App::new()
            .service(web::resource("/static/{tail:.*}").to(handlers::static_file_handler))
            .service(web::resource("/{tail:.*}").to_async(app_handler))
    };
    HttpServer::new(factory)
        .bind(address)
        .unwrap()
        .run()
        .unwrap();
}
