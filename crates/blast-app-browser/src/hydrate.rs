//! Hydration diffs the existing DOM, created from the HTML rendered on the server, against the
//! local vDOM.
//! Components are attached to existing elements, and any differences are patched and reported.
//! This lets the user start typing in text fields and scrolling through the page before the app
//! itself is done loading, without losing what they typed or the page jumping around.

use {
    log::warn,
    wasm_bindgen::JsCast,
    web_sys::{window, HtmlElement, Node},
};

use blast::{
    dom::{ActiveElement, ActiveNode, ElementContent},
    Blast, ComponentId,
};

use crate::{
    remove_node,
    render::render_tree,
    replace_node,
    state::{ActiveComponent, ComponentRoot, State},
};

pub(crate) fn hydrate_component(blast: &Blast, state: &mut State, id: ComponentId, target: &Node) {
    // Render the component itself
    let vdom = blast.component_vdom(id).unwrap();
    let root = hydrate_node(blast, state, &vdom, target);

    // Store it in the state
    state.active_components.insert(id, ActiveComponent { root });
}

fn hydrate_node(
    blast: &Blast,
    state: &mut State,
    node: &ActiveNode,
    target: &Node,
) -> ComponentRoot {
    match node {
        ActiveNode::Element(velement) => {
            let node = hydrate_velement(blast, state, node, velement, target);
            ComponentRoot::Real(node)
        }
        ActiveNode::Component(id) => {
            hydrate_component(blast, state, *id, target);
            ComponentRoot::Virtual(*id)
        }
        ActiveNode::Text(text) => {
            let node = hydrate_vtext(text, target);
            ComponentRoot::Real(node)
        }
    }
}

fn hydrate_velement(
    blast: &Blast,
    state: &mut State,
    node: &ActiveNode,
    velement: &ActiveElement,
    target: &Node,
) -> Node {
    // Validate the node type
    if target.node_type() != Node::ELEMENT_NODE {
        if target.node_type() == Node::TEXT_NODE {
            warn!("Hydration mismatch: Expected element DOM node, found text.");
        } else {
            warn!("Hydration mismatch: Expected element DOM node.");
        }

        // Replace the node with the right node
        let (new_node, _) = render_tree(blast, state, node);
        replace_node(target, &new_node);

        return new_node;
    }

    let element = target.dyn_ref::<HtmlElement>().unwrap();

    // Validate the element's tag, if they don't match we need to re-create
    let tag = element.tag_name().to_ascii_lowercase();
    let vtag = velement.tag.to_ascii_lowercase();
    if tag != vtag {
        warn!("Hydration mismatch: Expected <{}>, found <{}>", vtag, tag);

        // Replace the node with the right node
        let (new_node, _) = render_tree(blast, state, node);
        replace_node(target, &new_node);

        return new_node;
    }

    // TODO: Validate attributes

    match &velement.content {
        ElementContent::Children(vchildren) => {
            hydrate_children(blast, state, vchildren, target);
        }
        ElementContent::RawHtml(raw_html) => {
            let old_html = element.inner_html();

            // Not sure if this diffing approach works, but it's here for now
            if old_html != *raw_html {
                warn!("Hydration mismatch: Raw HTML is different from DOM.");
                element.set_inner_html(raw_html);
            }
        }
        ElementContent::Empty => {
            let children = target.child_nodes();
            let length = children.length() as usize;

            if length != 0 {
                warn!("Hydration mismatch: Expected empty element, found children.");
                for _ in 0..length {
                    let node = children.get(0).unwrap();
                    remove_node(&node);
                }
            }
        }
    }

    // We used the original node, so return that
    target.clone()
}

fn hydrate_children(
    blast: &Blast,
    state: &mut State,
    vchildren: &[ActiveNode],
    parent_node: &Node,
) {
    let children = parent_node.child_nodes();
    let mut max = (children.length() as usize).max(vchildren.len());
    let mut i = 0;

    while i < max {
        let node = vchildren.get(i);
        let target = children.get(i as u32);

        // Remove comments used to create space between text elements
        if target.as_ref().map(|n| n.node_type()) == Some(Node::COMMENT_NODE) {
            remove_node(&target.unwrap());

            // Because we've removed a node, reduce the max and do not increment
            max -= 1;

            continue;
        }

        if node.is_none() {
            warn!("Hydration mismatch: Excessive DOM node.");

            // Remove the excessive node
            let target = target.unwrap();
            remove_node(&target);

            // Because we've removed a node from the end, reduce the max and do not increment
            max -= 1;

            continue;
        }

        if target.is_none() {
            warn!("Hydration mismatch: Missing DOM node.");

            // Add the missing node
            let (new_node, _) = render_tree(blast, state, node.unwrap());
            parent_node.append_child(&new_node).unwrap();

            i += 1;

            continue;
        }

        hydrate_node(blast, state, node.unwrap(), &target.unwrap());
        i += 1;
    }
}

fn hydrate_vtext(text: &str, node: &Node) -> Node {
    // Validate the node type
    if node.node_type() != Node::TEXT_NODE {
        if node.node_type() == Node::ELEMENT_NODE {
            warn!("Hydration mismatch: Expected text DOM node, found element.");
        } else {
            warn!("Hydration mismatch: Expected text DOM node.");
        }

        // Create the correct node
        let document = window().unwrap().document().unwrap();
        let new_node = document.create_text_node(&text);

        // Replace the node with the right node
        replace_node(node, &new_node);

        return new_node.into();
    }

    // Validate text content
    let value = node.node_value().unwrap();
    if value != *text {
        warn!("Hydration mismatch: Text content does not match.");
        node.set_node_value(Some(&text));
    }

    node.clone()
}
