use std::{cell::RefCell, rc::Rc};

use {slotmap::SecondaryMap, web_sys::Node};

use blast::ComponentId;

pub type StateRef = Rc<RefCell<State>>;

pub struct State {
    pub active_components: SecondaryMap<ComponentId, ActiveComponent>,
}

impl State {
    pub fn new() -> Self {
        Self {
            active_components: SecondaryMap::new(),
        }
    }
}

pub struct ActiveComponent {
    pub root: ComponentRoot,
}

pub enum ComponentRoot {
    /// This component's root is rendered as a real node.
    Real(Node),
    /// This component's root is rendered as another component, and thus we don't own it.
    /// The ComponentId given here points to the component that does own the root.
    Virtual(ComponentId),
}

impl ComponentRoot {
    pub fn from_id_or_node(id: Option<ComponentId>, node: &Node) -> Self {
        if let Some(id) = id {
            ComponentRoot::Virtual(id)
        } else {
            ComponentRoot::Real(node.clone())
        }
    }
}