#![deny(bare_trait_objects)]
#![warn(clippy::all)]

mod hydrate;
mod render;
mod state;

use std::{cell::RefCell, rc::Rc};

use {
    futures::Future,
    log::debug,
    wasm_bindgen::JsValue,
    wasm_bindgen_futures::future_to_promise,
    web_sys::{window, Node},
};

use {
    blast::{diff::Diff, Blast, ComponentId, PlatformHandler, UnitFuture},
    blast_app::Init,
};

use crate::{
    hydrate::hydrate_component,
    render::render_tree,
    state::{ComponentRoot, State, StateRef},
};

pub fn start<I: Init>(init: &I) {
    // Initialize the app's data
    let (registry, root) = init.init();

    // Create a new Blast instance and add the app to it
    let state_ref = Rc::new(RefCell::new(State::new()));
    let blast = Blast::new(
        registry,
        ClientHandler {
            state_ref: state_ref.clone(),
        },
    );
    let id = blast.add_component(root);

    // Find the component to hydrate
    let document = window().unwrap().document().unwrap();
    let container = document.get_element_by_id("app").unwrap();
    let target = container.first_child().unwrap();

    // Hydrate the existing DOM with the app
    let mut state = state_ref.borrow_mut();
    hydrate_component(&blast, &mut state, id, &target);
}

struct ClientHandler {
    state_ref: StateRef,
}

impl PlatformHandler for ClientHandler {
    fn handle_future(&self, future: UnitFuture) {
        // We need to turn them back into promises to execute them
        let _promise = future_to_promise(
            future
                .map(|_| JsValue::UNDEFINED)
                .map_err(|_| JsValue::UNDEFINED),
        );
    }

    fn handle_diff(&self, blast: &Blast, diff: &Diff) {
        let mut state = self.state_ref.borrow_mut();

        for changed in &diff.changed {
            debug!(
                "Applying diff\n\
                 - {} Text node(s) changed\n\
                 - {} Node(s) replaced\n\
                 - {} Set(s) of children appended",
                changed.text_changed.len(),
                changed.nodes_replaced.len(),
                changed.children_appended.len()
            );

            // TODO: Text changes

            for node_replaced in &changed.nodes_replaced {
                // Find the active dom node to replace
                let current_node =
                    find_from_position(&state, changed.target, &node_replaced.position);

                // Render the new node tree
                let (dom_node, component_id) =
                    render_tree(blast, &mut state, &node_replaced.new_node);

                // Apply the render to the dom
                replace_node(&current_node, &dom_node);

                // If this was the root note, replace it in the entry
                if node_replaced.position.is_empty() {
                    let entry = state.active_components.get_mut(changed.target).unwrap();
                    entry.root = ComponentRoot::from_id_or_node(component_id, &dom_node);
                }
            }

            for children_appended in &changed.children_appended {
                // Find the active dom node to append to
                let current_node =
                    find_from_position(&state, changed.target, &children_appended.position);

                // Render and append the nodes one by one
                for new_child in &children_appended.new_children {
                    let (dom_node, _) = render_tree(blast, &mut state, &new_child);
                    current_node.append_child(&dom_node).unwrap();
                }
            }
        }
    }
}

fn find_from_position(state: &State, target: ComponentId, position: &[usize]) -> Node {
    let entry = state
        .active_components
        .get(target)
        .expect("Expected component with diff to be in active components");

    match &entry.root {
        ComponentRoot::Real(node) => {
            let mut current_node = node.clone();

            for i in position {
                let children = current_node.child_nodes();

                // TODO: A plugin or the user could be messing with the page, in this case this
                // may fail. When that happens, we probably want to re-render the entire
                // component.
                current_node = children
                    .get(*i as u32)
                    .expect("DOM was modified externally");
            }

            current_node
        }
        ComponentRoot::Virtual(id) => {
            // In this case, we can only accept positions for the root.
            // If diffing is working right that's the only value we should get.
            assert!(
                position.is_empty(),
                "Position wasn't for root, when root is required for ComponentRoot::Virtual",
            );

            // Recursively find the dom node we're looking for
            find_real(state, *id)
        }
    }
}

fn find_real(state: &State, id: ComponentId) -> Node {
    let entry = state.active_components.get(id).unwrap();
    match &entry.root {
        ComponentRoot::Real(node) => node.clone(),
        ComponentRoot::Virtual(id) => find_real(state, *id),
    }
}

fn replace_node(old: &Node, new: &Node) {
    let parent = old.parent_node().unwrap();
    parent.replace_child(new, old).unwrap();
}

fn remove_node(node: &Node) {
    let parent = node.parent_node().unwrap();
    parent.remove_child(&node).unwrap();
}
