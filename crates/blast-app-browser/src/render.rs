//! As opposed to hydration, rendering creates a DOM tree from scratch.
//! This is used when hydration mismatches are found, or when a new sub-tree is inserted by a diff.

use web_sys::{window, Node};

use blast::{
    dom::{ActiveNode, ElementContent},
    Blast, ComponentId,
};

use crate::state::{ActiveComponent, ComponentRoot, State};

/// If the node given is immediately a component, returns the new component's ID.
/// This second return value is used to correctly create the ComponentRoot.
pub fn render_tree(
    blast: &Blast,
    state: &mut State,
    node: &ActiveNode,
) -> (Node, Option<ComponentId>) {
    let document = window().unwrap().document().unwrap();

    match node {
        ActiveNode::Element(element) => {
            let dom_element = document.create_element(&element.tag).unwrap();

            // Set the attributes
            for (key, value) in &element.attributes {
                dom_element.set_attribute(key, value).unwrap();
            }

            // Add the content
            match &element.content {
                ElementContent::Children(children) => {
                    for child in children {
                        let (dom_node, _) = render_tree(blast, state, child);
                        dom_element.append_with_node_1(&dom_node).unwrap();
                    }
                }
                ElementContent::RawHtml(html) => dom_element.set_inner_html(html),
                ElementContent::Empty => {}
            }

            (dom_element.into(), None)
        }
        ActiveNode::Component(component) => {
            // Get the dom tree for this component from blast
            let node = blast.component_vdom(*component).unwrap();

            // Render it
            let (dom_node, component_id) = render_tree(blast, state, &node);

            // Store the component and node so we can keep track of it.
            // In a new tree we can always assume this is a new component, we do not need to check
            // if it already exists.
            let value = state.active_components.insert(
                *component,
                ActiveComponent {
                    root: ComponentRoot::from_id_or_node(component_id, &dom_node),
                },
            );
            assert!(value.is_none());

            // Return the rendered node, it will be added by whatever we're called by
            (dom_node, Some(*component))
        }
        ActiveNode::Text(text) => {
            let node = document.create_text_node(&text).into();
            (node, None)
        }
    }
}
